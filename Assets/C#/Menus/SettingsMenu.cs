﻿using Assets.C_.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.C_.Menus
{
    public class SettingsMenu : MonoBehaviour
    {
        private static GameObject pausePan,
        settingsPan;

        private static GameWrapper gw;

        public static void LoadSettingsObjs(GameObject pPan, GameObject sPan, GameWrapper _gw)
        {
            pausePan = pPan;

            settingsPan = sPan;

            gw = _gw;
        }

        public void ReturnClick()
        {
            if (gw.IsSoundOn)
                gw.Fxs.ClickSrc.PlayOneShot(gw.Fxs.ClickSrc.clip);

            pausePan.SetActive(true);

            settingsPan.SetActive(false);
        }

        public void SoundClick()
        {
            PauseMenu.SoundClick(settingsPan);
        }

        public void MusicClick()
        {
            if (gw.IsSoundOn)
                gw.Fxs.ClickSrc.PlayOneShot(gw.Fxs.ClickSrc.clip);

            PauseMenu.MusicClick(settingsPan);
        }
    }
}
