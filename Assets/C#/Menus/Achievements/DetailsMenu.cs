﻿using Assets.C_.Game;
using Assets.C_.Game.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.C_.Menus.Achievements
{
    public class DetailsMenu : MonoBehaviour
    {
        private static GameObject pausePan,
        achiDetailPan,
        achiPan;

        private static GameWrapper gw;

        private static Text descLbl,
            achiLbl,
            howToLbl;

        public static void LoadData(GameObject _achiDetailPan, GameObject _achiPan, GameObject _pausePan, GameWrapper _gw, 
            Button btn)
        {
            string btnTxt = (btn.GetComponentInChildren<Text>()).text;

            achiDetailPan = _achiDetailPan;

            achiPan = _achiPan;

            pausePan = _pausePan;

            gw = _gw;

            #region Setup UI controls

            #region Intantiations

            Text[] childrenComp = achiDetailPan.GetComponentsInChildren<Text>();

            achiLbl = childrenComp[0];

            descLbl = childrenComp[1];

            howToLbl = childrenComp[2];

            #endregion

            #region Change controls according to achievement

            achiLbl.text = string.Format("../{0}", btnTxt.Replace("{", string.Empty).Replace("}", string.Empty));

            descLbl.text = "/* ";

            howToLbl.text = string.Format("/* Objective -----{0}", Environment.NewLine);

            if (btn.name.ToLower().Contains(Achievement.Titles.COOKIE.ToString().ToLower()))
            {         
                descLbl.text += Achievement.COOKIE_DESC;

                howToLbl.text += Achievement.COOKIE_HOW_TO;
            }

            else if (btn.name.ToLower().Contains(Achievement.Titles.FLAME4.ToString().ToLower()))
            {
                descLbl.text += Achievement.FLAME4_DESC;

                howToLbl.text += Achievement.FLAME4_HOW_TO;
            }

            else if (btn.name.ToLower().Contains(Achievement.Titles.FLAME3.ToString().ToLower()))
            {
                descLbl.text += Achievement.FLAME3_DESC;

                howToLbl.text += Achievement.FLAME3_HOW_TO;
            }

            else if (btn.name.ToLower().Contains(Achievement.Titles.FLAME2.ToString().ToLower()))
            {
                descLbl.text += Achievement.FLAME2_DESC;

                howToLbl.text += Achievement.FLAME2_HOW_TO;
            }

            else if (btn.name.ToLower().Contains(Achievement.Titles.FLAME1.ToString().ToLower()))
            {
                descLbl.text += Achievement.FLAME1_DESC;

                howToLbl.text += Achievement.FLAME1_HOW_TO;
            }

            else if (btn.name.ToLower().Contains(Achievement.Titles.FLAME.ToString().ToLower()))
            {
                descLbl.text += Achievement.FLAME_DESC;

                howToLbl.text += Achievement.FLAME_HOW_TO;
            }

            else if (btn.name.ToLower().Contains(Achievement.Titles.STREAK4.ToString().ToLower()))
            {
                descLbl.text += Achievement.STREAK4_DESC;

                howToLbl.text += Achievement.STREAK4_HOW_TO;
            }

            else if (btn.name.ToLower().Contains(Achievement.Titles.STREAK3.ToString().ToLower()))
            {
                descLbl.text += Achievement.STREAK3_DESC;

                howToLbl.text += Achievement.STREAK3_HOW_TO;
            }

            else if (btn.name.ToLower().Contains(Achievement.Titles.STREAK2.ToString().ToLower()))
            {
                descLbl.text += Achievement.STREAK2_DESC;

                howToLbl.text += Achievement.STREAK2_HOW_TO;
            }

            else if (btn.name.ToLower().Contains(Achievement.Titles.STREAK.ToString().ToLower()))
            {
                descLbl.text += Achievement.STREAK_DESC;

                howToLbl.text += Achievement.STREAK_HOW_TO;
            }

            //Wait
            else
            {
                descLbl.text += Achievement.WAIT_DESC;

                howToLbl.text += Achievement.WAIT_HOW_TO;
            }

            descLbl.text += " */";

            howToLbl.text += " */";

            #endregion

            #endregion
        }

        public void ReturnClick(Button btn)
        {
            if (gw.IsSoundOn)
                gw.Fxs.ClickSrc.PlayOneShot(gw.Fxs.ClickSrc.clip);

            achiDetailPan.SetActive(false);

            achiPan.SetActive(!achiDetailPan.activeSelf);

            AchiPan.LoadList(pausePan, achiPan, achiDetailPan, gw);
        }
    }
}
