﻿using Assets.C_.Game;
using Assets.C_.Game.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuCanvas : MonoBehaviour
{
    //public static bool hasStarted = false;

    private static GameWrapper gw = new GameWrapper();

    private Animatron anim,
        lblAnim,
        initialLblAnim,
        initialTxtBxAnim,
        saveBtnAnim,
        initialTxtBxLblAnim;

    private int frameCount;

    private GameObject MainMenuPanel;

    private Image img; //Panel background Img

    private Text lbl,
        initialsLbl,
        saveBtnLbl,
        initialTxtBxLbl;

    private InputField initialTxtBx;

    private Button saveBtn;

    //private bool saveIsClicked = false;

    private void Awake()
    {
         MainMenuPanel = GameObject.Find("/MainMenuCanvas/MainMenuPanel");

        lbl = GameObject.Find("/MainMenuCanvas/MainMenuPanel/Title3Lbl").GetComponent<Text>();

        initialsLbl = GameObject.Find("/MainMenuCanvas/MainMenuPanel/InitialsLbl").GetComponent<Text>();

        initialTxtBx = GameObject.Find("/MainMenuCanvas/MainMenuPanel/InitialsTxtBx").GetComponent<InputField>();

        initialTxtBxLbl = initialTxtBx.GetComponentsInChildren<Text>()[1];

        saveBtn = GameObject.Find("/MainMenuCanvas/MainMenuPanel/SaveBtn").GetComponent<Button>();

        gw.Fxs.ClickSrc = GameObject.Find("BtnSrc").GetComponent<AudioSource>();

        //This is how you add a method to the event handler
        saveBtn.onClick.AddListener(() => SaveBtnClick());

        saveBtnLbl = saveBtn.GetComponentsInChildren<Text>()[0];

        img = MainMenuPanel.GetComponent<Image>();

        #region Init animatron objs

        anim = new Animatron();

        lblAnim = new Animatron()
        {
            InitialAlpha = 1f,

            Red = lbl.color.r,

            Green = lbl.color.g,

            Blue = lbl.color.b
        };

        initialLblAnim = new Animatron()
        {
            InitialAlpha = 1f,

            Red = lbl.color.r,

            Green = lbl.color.g,

            Blue = lbl.color.b
        };

        initialTxtBxAnim = new Animatron()
        {
            InitialAlpha = 1f,

            Red = lbl.color.r,

            Green = lbl.color.g,

            Blue = lbl.color.b
        };

        saveBtnAnim = new Animatron()
        {
            InitialAlpha = 1f,

            Red = lbl.color.r,

            Green = lbl.color.g,

            Blue = lbl.color.b
        };

        initialTxtBxLblAnim = new Animatron()
        {
            InitialAlpha = 1f,

            Red = lbl.color.r,

            Green = lbl.color.g,

            Blue = lbl.color.b
        }; 

        #endregion
    }

    private void Update()
    {
        //Chnage visibility for main menu canvas when the animations are over
        if (anim.AnimationEnded && !anim.AnimationRunning)
        {
            if (!gw.HasStarted)
            {
                //gw.UserInitials = initialTxtBx.text;

                gw.UserInitials = string.IsNullOrEmpty(initialTxtBx.text) ? "MAP" : initialTxtBx.text;

                gw.HasStarted = true;

                MainMenuPanel.SetActive(false);
            }
        }

        //Fade out main menu UI components
        else if(!anim.AnimationEnded && anim.AnimationRunning)
        {
            //++ Before cause I need it to add when it's stepping in the method not when it's stepping out
            img.color = anim.FadeOutObject(++frameCount, img.color);

            lbl.color = lblAnim.FadeOutObject(frameCount, lbl.color, 0.100f);

            initialsLbl.color = initialLblAnim.FadeOutObject(frameCount, initialsLbl.color, 0.100f);

            saveBtnLbl.color = saveBtnAnim.FadeOutObject(frameCount, saveBtnLbl.color, 0.100f);

            #region Set textbox normal color

            //Gotta do it like this cause can't change the normal color straight from the txtbx
            ColorBlock cb = initialTxtBx.colors;

            cb.normalColor = initialTxtBxAnim.FadeOutObject(frameCount, initialTxtBx.colors.normalColor, 0.100f);

            initialTxtBx.colors = cb;
             
            #endregion

            initialTxtBxLbl.color = initialTxtBxLblAnim.FadeOutObject(frameCount, initialTxtBxLbl.color, 0.100f);
        }
    }

    private void OnDestroy()
    {
        saveBtn.onClick.RemoveAllListeners();
    }

    public static GameWrapper GetWrapper()
    {
        return gw;
    }

    public void SaveBtnClick()
    {
        //saveIsClicked = !saveIsClicked;

        gw.Fxs.ClickSrc.PlayOneShot(gw.Fxs.ClickSrc.clip);

        anim.Start();

        lblAnim.Start();

        initialLblAnim.Start();

        initialTxtBxAnim.Start();

        saveBtnAnim.Start();

        initialTxtBxLblAnim.Start();
    }
}
