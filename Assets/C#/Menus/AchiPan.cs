﻿using Assets.C_.Game;
using Assets.C_.Game.Models;
using Assets.C_.Menus.Achievements;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AchiPan : MonoBehaviour
{
    private static GameObject pausePan,
        achiDetailPan,
        achiPan;

    private static GameWrapper _gw;

    private static Button cookieBtn,
        waitBtn,
        streakBtn,
        streak2Btn,
        streak3Btn,
        streak4Btn,
        flameBtn4,
        flameBtn,
        flameBtn1,
        flameBtn2,
        flameBtn3;
    
    //public static void LoadList(GameObject pausePanObj, GameObject achiPanObj, GameWrapper gw)
    public static void LoadList(GameObject pausePanObj, GameObject achiPanObj,
        GameObject achiDetailPanObj, GameWrapper gw)
    {
        achiPan = achiPanObj;

        #region Instantiate btns

        cookieBtn = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.COOKIE];

        waitBtn = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.WAIT];

        streakBtn = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.STREAK];

        streak2Btn = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.STREAK2];

        streak3Btn = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.STREAK3];

        streak4Btn = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.STREAK4];

        flameBtn = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.FLAME];

        flameBtn1 = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.FLAME1];

        flameBtn2 = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.FLAME2];

        flameBtn3 = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.FLAME3];

        flameBtn4 = achiPan.GetComponentsInChildren<Button>()[(int)Achievement.Titles.FLAME4];

        #endregion

        _gw = gw;

        #region Set enabled value for btns

        Achievement achs = _gw.Achievements;

        if (achs.UnlockedCookie)
        {
            cookieBtn.interactable = true;

            Text txt = cookieBtn.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.COOKIE_TITLE;

            //(cookieBtn.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }

        if (achs.UnlockedFlame)
        {
            flameBtn.interactable = true;

            Text txt = flameBtn.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.FLAME_TITLE;

            //(flameBtn.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }

        if (achs.UnlockedFlame1)
        {
            flameBtn1.interactable = true;

            Text txt = flameBtn1.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.FLAME1_TITLE;

            //(flameBtn1.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }

        if (achs.UnlockedFlame2)
        {
            flameBtn2.interactable = true;

            Text txt = flameBtn2.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.FLAME2_TITLE;

            //(flameBtn2.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }

        if (achs.UnlockedFlame3)
        {
            flameBtn3.interactable = true;

            Text txt = flameBtn3.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.FLAME3_TITLE;

            //(flameBtn3.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }

        if (achs.UnlockedFlame4)
        {
            flameBtn4.interactable = true;

            Text txt = flameBtn4.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.FLAME4_TITLE;

            //(flameBtn4.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }

        if (achs.UnlockedStreak)
        {
            streakBtn.interactable = true;

            Text txt = streakBtn.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.STREAK_TITLE;

            //(streakBtn.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }

        if (achs.UnlockedStreak2)
        {
            streak2Btn.interactable = true;

            Text txt = streak2Btn.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.STREAK2_TITLE;

            //(streak2Btn.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }

        if (achs.UnlockedStreak3)
        {
            streak3Btn.interactable = true;

            Text txt = streak3Btn.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.STREAK3_TITLE;

            //(streak3Btn.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }        

        if (achs.UnlockedStreak4)
        {
            streak4Btn.interactable = true;

            Text txt = streak4Btn.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.STREAK4_TITLE;

            //(streak4Btn.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        }

        if (achs.UnlockedWait)
        {
            waitBtn.interactable = true;

            Text txt = waitBtn.GetComponentInChildren<Text>();

            txt.color = new Color(1, 1, 1, 1);

            txt.text = Achievement.WAIT_TITLE;

            //(waitBtn.GetComponentInChildren<Text>()).color = new Color(1, 1, 1, 1);
        } 

        #endregion

        pausePan = pausePanObj;

        achiDetailPan = achiDetailPanObj;
    }

    public void BackClick()
    {
        if(_gw.IsSoundOn)
            _gw.Fxs.ClickSrc.PlayOneShot(_gw.Fxs.ClickSrc.clip);

        achiPan.SetActive(false);

        pausePan.SetActive(true);
    }

    public void AchiClick(Button btn)
    {
        if (_gw.IsSoundOn)
            _gw.Fxs.ClickSrc.PlayOneShot(_gw.Fxs.ClickSrc.clip);

        achiPan.SetActive(false);

        achiDetailPan.SetActive(!achiPan.activeSelf);

        DetailsMenu.LoadData(achiDetailPan, achiPan, pausePan, _gw, btn);
    }
}
