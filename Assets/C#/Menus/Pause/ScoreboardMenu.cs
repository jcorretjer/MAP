﻿using Assets.C_.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.C_.Menus.Pause
{
    public class ScoreboardMenu : MonoBehaviour
    {
        private static GameObject pausePan,
        scorePan;

        private static GameWrapper wrapper;

        public static void LoadScores(GameObject _pausePan, GameObject _scorePan, GameWrapper _wrapper)
        {
            pausePan = _pausePan;

            scorePan = _scorePan;

            wrapper = _wrapper;

            Button[] childrenComp = scorePan.GetComponentsInChildren<Button>();

            ( childrenComp[0].GetComponentInChildren<Text>()).text = string.Format("//1. {0} - {1}",
                wrapper.Scoreboard.ElementAt(0).Value, wrapper.Scoreboard.ElementAt(0).Key.Replace("*", string.Empty));

            var i1 = ( childrenComp[0].GetComponentInChildren<Text>() ).text;

            if (wrapper.Scoreboard.Count > 1)
                (childrenComp[1].GetComponentInChildren<Text>()).text = string.Format("//2. {0} - {1}",
                    wrapper.Scoreboard.ElementAt(1).Value, wrapper.Scoreboard.ElementAt(1).Key.Replace("*", string.Empty));

            else
                (childrenComp[1].GetComponentInChildren<Text>()).text = string.Empty;

            i1 = ( childrenComp[1].GetComponentInChildren<Text>() ).text;

            if (wrapper.Scoreboard.Count > 2)
                (childrenComp[2].GetComponentInChildren<Text>()).text = string.Format("//3. {0} - {1}",
                wrapper.Scoreboard.ElementAt(2).Value, wrapper.Scoreboard.ElementAt(2).Key.Replace("*", string.Empty));

            else
                (childrenComp[2].GetComponentInChildren<Text>()).text = string.Empty;
        }

        public void ReturnClick()
        {
            if (wrapper.IsSoundOn)
                wrapper.Fxs.ClickSrc.PlayOneShot(wrapper.Fxs.ClickSrc.clip);

            scorePan.SetActive(false);

            pausePan.SetActive(!scorePan.activeSelf);
        }
    }
}
