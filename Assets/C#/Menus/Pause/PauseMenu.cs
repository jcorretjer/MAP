﻿using Assets.C_.Ball.Models;
using Assets.C_.Ball.Services;
using Assets.C_.Game;
using Assets.C_.Game.Models;
using Assets.C_.Menus;
using Assets.C_.Menus.Pause;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    //private static Ball _ball; //Used to get the max speed once app is killed

    private static Button musicBtn,
    soundBtn;

    private static GameWrapper wrapper;

    private static GameObject pausePan,
        settingsPan,
        achiDetailPan,
        scorePan,
        achiPan,
        assistPan;

    public void ResumeClick()
    {
        if (wrapper.IsSoundOn)
            wrapper.Fxs.ClickSrc.PlayOneShot(wrapper.Fxs.ClickSrc.clip);

        //Resume time
        Time.timeScale = 1f;

        pausePan.SetActive(false);
    }

    /// <summary>
    /// This is the only way to call this class so it works like a constructor
    /// </summary>
    /// <param name="ball"></param>
    /// <param name="pausePanObj"></param>
    //public static void PauseClick(Ball ball, GameObject pausePanObj, GameObject achiPanObj)
    public static void PauseClick(GameObject pausePanObj, GameObject achiPanObj, GameObject settingsPanObj,
        GameObject achiDetailPanObj, GameObject _scorePan, GameObject _assistPan)
    {
        //_ball = ball;

        wrapper = BallMain.GetGameWrapper();

        pausePan = pausePanObj;

        settingsPan = settingsPanObj;

        musicBtn = pausePan.GetComponentsInChildren<Button>()[1];

        soundBtn = pausePan.GetComponentsInChildren<Button>()[2];

        //Pause time
        Time.timeScale = 0f;

        pausePan.SetActive(true);

        achiPan = achiPanObj;

        achiDetailPan = achiDetailPanObj;

        scorePan = _scorePan;

        assistPan = _assistPan;
    }

    public static void PauseClick(GameObject pausePanObj, GameObject settingsPanObj)
    {
        //_ball = ball;

        wrapper = BallMain.GetGameWrapper();

        pausePan = pausePanObj;

        settingsPan = settingsPanObj;

        //musicBtn = pausePan.GetComponentsInChildren<Button>()[1];

        //soundBtn = pausePan.GetComponentsInChildren<Button>()[2];

        //Pause time
        Time.timeScale = 0f;

        pausePan.SetActive(true);
    }

    /// <summary>
    /// Very similar to musicclick but with sound. This cause it won't play or stop cause fx play once
    /// </summary>
    public static void SoundClick(GameObject sPanel)
    {
        wrapper.IsSoundOn = !wrapper.IsSoundOn;

        soundBtn = sPanel.GetComponentsInChildren<Button>()[2];

        Text txt = soundBtn.GetComponentInChildren<Text>();

        if (wrapper.IsSoundOn)
        {
            if (txt.text.Contains("OFF"))
                txt.text = txt.text.Replace("OFF", "ON");

            wrapper.Fxs.ClickSrc.PlayOneShot(wrapper.Fxs.ClickSrc.clip);
        }

        else
        {
            if (txt.text.Contains("ON"))
                txt.text = txt.text.Replace("ON", "OFF");
        }

        BallMain.SetGameWrapper(wrapper);

    }

    /// <summary>
    /// Get Audio src for music, play it according to flag and set text according to play flag
    /// </summary>
    public static void MusicClick(GameObject sPanel)
    {
        wrapper.isMusicOn = !wrapper.isMusicOn;

        musicBtn = sPanel.GetComponentsInChildren<Button>()[1];

        AudioSource src = BallMain.GetMusicAudioSrc();

        Text txt = musicBtn.GetComponentInChildren<Text>();

        if (wrapper.isMusicOn)
        {
            if (!src.isPlaying)
            {
                if (txt.text.Contains("OFF"))
                    txt.text = txt.text.Replace("OFF", "ON");

                src.Play();
            }
        }

        else
        {
            if (txt.text.Contains("ON"))
                txt.text = txt.text.Replace("ON", "OFF");

            src.Pause();
        }

        BallMain.SetGameWrapper(wrapper);
    }

    /// <summary>
    /// Save data and Kill the app
    /// </summary>
    public void QuitClick()
    {
        if (wrapper.IsSoundOn)
            wrapper.Fxs.ClickSrc.PlayOneShot(wrapper.Fxs.ClickSrc.clip);

        string scoreboardFileTxt = string.Empty,
            achievementsFileTxt = string.Empty;

        //Same as OnApplicationPause for BallMain
        for (int i = 0; i < wrapper.Scoreboard.Count; i++)
        {
            scoreboardFileTxt += string.Format("{0}-{1};", wrapper.Scoreboard.ElementAt(i).Value, wrapper.Scoreboard.ElementAt(i).Key.Trim());
        }

        achievementsFileTxt = BallMain.ConvertAchievementsToString(wrapper.Achievements);

        string encryptedScore = Convert.ToBase64String(Cryptonic.EncryptString(scoreboardFileTxt, Cryptonic.Key)),
                encryptedAchiv = Convert.ToBase64String(Cryptonic.EncryptString(achievementsFileTxt, Cryptonic.Key));

        //encrypted = Cryptonic.DecryptString(Convert.FromBase64String(encrypted), Cryptonic.Key);

        FileManager.Write(encryptedScore, Force.SAVE_FILE_NAME);

        FileManager.Write(encryptedAchiv, Achievement.SAVE_FILE_NAME);

        Application.Quit();
    }

    public void AchiClick()
    {
        if (wrapper.IsSoundOn)
            wrapper.Fxs.ClickSrc.PlayOneShot(wrapper.Fxs.ClickSrc.clip);

        pausePan.SetActive(false);

        achiPan.SetActive(true);

        //scrollPan.SetActive(true);

        AchiPan.LoadList(pausePan, achiPan, achiDetailPan, wrapper);

        //AchiPan.LoadList(pausePan, achiPan, scrollPan);
    }

    public void SettingsClick()
    {
        if (wrapper.IsSoundOn)
            wrapper.Fxs.ClickSrc.PlayOneShot(wrapper.Fxs.ClickSrc.clip);

        pausePan.SetActive(false);

        settingsPan.SetActive(true);

        SettingsMenu.LoadSettingsObjs(pausePan, settingsPan, wrapper);
    }

    public void ScoreClick()
    {
        if (wrapper.IsSoundOn)
            wrapper.Fxs.ClickSrc.PlayOneShot(wrapper.Fxs.ClickSrc.clip);

        pausePan.SetActive(false);

        scorePan.SetActive(!pausePan.activeSelf);

        ScoreboardMenu.LoadScores(pausePan, scorePan, wrapper);
    }

    public void AssistClick()
    {
        if (wrapper.IsSoundOn)
            wrapper.Fxs.ClickSrc.PlayOneShot(wrapper.Fxs.ClickSrc.clip);

        assistPan.SetActive(!assistPan.activeSelf);
    }
}
