﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.C_.Game.Models
{
    public class Achievement
    {
        public enum Titles
        {
            COOKIE = 1,
            WAIT,
            FLAME,
            FLAME1,
            FLAME2,
            FLAME3,
            FLAME4,
            STREAK,
            STREAK2,
            STREAK3,
            STREAK4            
        }

        public enum SaveFileOrder
        {
            COOKIE = 0,
            FLAME,
            FLAME1,
            FLAME2,
            FLAME3,
            FLAME4,
            STREAK,
            STREAK2,
            STREAK3,
            STREAK4,
            WAIT
        }

        public const string COOKIE_DESC = "Wow, congrats cadet, you somehow figured out that you can tap on that  moving object.",
            WAIT_DESC = "You aren’t taking the program to sit and stare at the screen. Consider this a notice of your inaction.",
            FLAME_DESC = "Glad to see you’re physiology has adapted to the Nano probes. Keep up the good work",
            FLAME1_DESC = "You keep surprising us. And here we had painted the wall at the end of the hall in hopes you’ll careen into it.",
            FLAME2_DESC = "Maybe now cadet you are fast enough to save all your furry friends.",
            FLAME3_DESC = "Impressive cadet. We will proceed to removed the oil trap we’ve set around your station.",
            FLAME4_DESC = "In order to avoid technical failures, please keep your velocity below the speed limit which is 671 million mph.",
            STREAK_DESC = "Your accuracy is unimpressive at best and lacking in anything worth appraisal at worst.",
            STREAK2_DESC = "You may notice a slight burning sensation in the fingers, this is a normal aftereffect of the procedure.",
            STREAK3_DESC = "If you’ve noticed improvement in your accuracy it is due to high command order to replaced your fingers with that of a previous recruits.",
            STREAK4_DESC = "High command is so entertained by you, it’s been requested you attend the officers dinner to be a spectacle.",

            COOKIE_TITLE = "{ Take a Cookie }",
            WAIT_TITLE = "{ Wait for It... }",
            FLAME_TITLE = "{ Going Turbo! }",
            FLAME1_TITLE = "{ Beep, Beep! }",
            FLAME2_TITLE = "{ Gotta Go Fast! }",
            FLAME3_TITLE = "{ Racer X }",
            FLAME4_TITLE = "{ The Speed Force }",
            STREAK_TITLE = "{ You Got This }",
            STREAK2_TITLE = "{ Feel the Burn }",
            STREAK3_TITLE = "{ Franken Fingers }",
            STREAK4_TITLE = "{ Tap Dancer }",

            COOKIE_HOW_TO = "Hit your 1st ball",
            WAIT_HOW_TO = "Wait 20 secs and then hit the ball",
            FLAME_HOW_TO = "Reach 10 MPH",
            FLAME1_HOW_TO = "Reach 30 MPH",
            FLAME2_HOW_TO = "Reach 60 MPH",
            FLAME3_HOW_TO = "Reach 80 MPH",
            FLAME4_HOW_TO = "Reach 100 MPH",
            STREAK_HOW_TO = "Perform 5 consecutive hits for the 1st streak (x1)",
            STREAK2_HOW_TO = "Reach a streak of 4 (x4)",
            STREAK3_HOW_TO = "Reach a streak of 5 (x5)",
            STREAK4_HOW_TO = "Reach a streak of 6 (x6)",

            SAVE_FILE_NAME = "Achievements";

        public const int WAIT_DURATION = 20, //Seconds that must pass in order to unlock wait achievement

            HITS_TO_STREAK = 5, //Amount of consecutive hits to start a streak
            TOTAL_FOR_STREAK = 1, //Total of streaks to unlock achievment
            TOTAL_FOR_STREAK2 = 4,
            TOTAL_FOR_STREAK3 = 5,
            TOTAL_FOR_STREAK4 = 6,

            DURATION_FLICKER = 300,
            FLICKER_INDICATOR = 30;

        public Achievement()
        {
            UnlockedCookie = false;

            UnlockedWait = false;

            UnlockedStreak = false;

            UnlockedStreak2 = false;

            UnlockedStreak3 = false;

            UnlockedStreak4 = false;

            UnlockedFlame = false;

            UnlockedFlame1 = false;

            UnlockedFlame2 = false;

            UnlockedFlame3 = false;

            UnlockedFlame4 = false;

            StreakCounter = 0;

            AccumulatedStreak = 0;
        }

        public Achievement(Achievement ach)
        {
            AccumulatedStreak = ach.AccumulatedStreak;

            UnlockedCookie = ach.UnlockedCookie;

            UnlockedWait = ach.UnlockedWait;

            UnlockedStreak = ach.UnlockedStreak;

            UnlockedStreak2 = ach.UnlockedStreak2;

            UnlockedStreak3 = ach.UnlockedStreak3;

            UnlockedStreak4 = ach.UnlockedStreak4;

            UnlockedFlame = ach.UnlockedFlame;

            UnlockedFlame1 = ach.UnlockedFlame1;

            UnlockedFlame2 = ach.UnlockedFlame2;

            UnlockedFlame3 = ach.UnlockedFlame3;

            UnlockedFlame4 = ach.UnlockedFlame4;

            StreakCounter = ach.StreakCounter;
        }

        public int AccumulatedStreak
        {
            get;

            set;
        }

        /// <summary>
        /// Current running frame count after achievement was unlocked
        /// </summary>
        public int ActiveCurrentFrameCount
        {
            get;

            set;
        }

        /// <summary>
        /// //The last frame count before it changes flicker
        /// </summary>
        public int ActivePreviousFrameCount
        {
            get;

            set;
        }

        public int StreakCounter
        {
            get;

            set;
        }

        public bool UnlockedCookie
        {
            get;

            set;
        }

        public bool UnlockedWait
        {
            get;

            set;
        }

        public bool UnlockedStreak
        {
            get;

            set;
        }

        public bool UnlockedStreak2
        {
            get;

            set;
        }

        public bool UnlockedStreak3
        {
            get;

            set;
        }

        public bool UnlockedStreak4
        {
            get;

            set;
        }

        public bool UnlockedFlame
        {
            get;

            set;
        }

        public bool UnlockedFlame1
        {
            get;

            set;
        }

        public bool UnlockedFlame2
        {
            get;

            set;
        }

        public bool UnlockedFlame3
        {
            get;

            set;
        }

        public bool UnlockedFlame4
        {
            get;

            set;
        }
    }
}
