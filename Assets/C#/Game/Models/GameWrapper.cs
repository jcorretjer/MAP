﻿using Assets.C_.Ball.Services.FXs;
using Assets.C_.Game.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.C_.Game
{
    /// <summary>
    /// Contains all the main properties of the game
    /// </summary>
    public class GameWrapper
    {
        public const double BUILD_NUMBER = 2.3;

        public enum Scenes
        {
            Splash,
            Main
        }

        public enum MouseButtons
        {
            Left,
            Right
        }

        public GameWrapper()
        {
            HasStarted = false;

            isMusicOn = true;

            IsSoundOn = true;

            Scoreboard = new Dictionary<string, int>();

            Achievements = new Achievement();

            Fxs = new FXs();
        }

        public string UserInitials
        {
            get;

            set;
        }

        public bool HasStarted
        {
            get;

            set;
        }

        public bool IsSoundOn
        {
            get;

            set;
        }

        public bool isMusicOn
        {
            get;

            set;
        }

        public FXs Fxs
        {
            get;

            set;
        }

        public Dictionary<string, int> Scoreboard
        {
            get;

            set;
        }

        public Achievement Achievements
        {
            get;

            set;
        }

        /// <summary>
        /// Converts an int to a decimal. Example if you got 5, it becomes 0.5
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static float IntToFloat(int num)
        {
            return (float)(num * 0.1);
        }
    }
}
