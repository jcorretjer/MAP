﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.C_.Ball.Services
{
    public class Animations : MonoBehaviour
    {
        private Animator anim;

            /// <summary>
            /// Switch back to a previous animation
            /// </summary>
        public void EndAnimation()
        {
            anim = GetComponent<Animator>();

            anim.SetBool("PlayBurst", false);
        }
    }
}
