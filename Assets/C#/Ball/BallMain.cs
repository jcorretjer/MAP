﻿using Assets.C_.Ball.Models;
using Assets.C_.Ball.Services;
using Assets.C_.Ball.Services.FXs;
using Assets.C_.Game;
using Assets.C_.Game.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;


public class BallMain : MonoBehaviour
{
    private Ball ball;

    private static List<AudioSource> audioSrcs;

    private static AudioSource musicSrc;

    private Text maxSpeedlbl,
        currentSpeedLbl,
        logLbl,
        streakLbl,
        archiveLbl;

    private FXs fxs;

    private static GameWrapper wrapper = null;

    private Dictionary<string, int> scoreboard = new Dictionary<string, int>();

    private Achievement achievements = new Achievement();

    private GameObject pausePan,
        settingsPan,
        achiDetailPan,
        scorePan,
        achiPan,
        assistPan;

    private Animator burstAnim;

    private int hitsCounter = 0, //Counts the amount of hits before speed drops
        archiveUnlockCounter = 0; //Counts the amount of achiviements that were unlocked

    private DateTime currentDateAfter30secs = DateTime.MaxValue,
        currentDate;

    private string output = string.Empty,
        stack = string.Empty;

    // Use this for initialization
    void Awake ()
    {
        #region Initiations

        #region Read saved file

        //string scoreboardTxt = string.IsNullOrEmpty(FileManager.Read(Force.SAVE_FILE_NAME)) ? "5-MAP;"
        string scoreboardTxt = string.IsNullOrEmpty(FileManager.Read(Force.SAVE_FILE_NAME)) ? string.Empty
               : FileManager.Read(Force.SAVE_FILE_NAME),
               achivementTxt = string.IsNullOrEmpty(FileManager.Read(Achievement.SAVE_FILE_NAME)) ? string.Empty
               : FileManager.Read(Achievement.SAVE_FILE_NAME);

        #region Scoreboard

        if (string.IsNullOrEmpty(scoreboardTxt))
            scoreboardTxt = "5-MAP;";

        else
            scoreboardTxt = Cryptonic.DecryptString(Convert.FromBase64String(scoreboardTxt), Cryptonic.Key);

        //All scores are separeted by ;
        string[] scoreboardSplit = scoreboardTxt.Split(';');

        /*
         * Once you have all the scores in an array, iterate through all scores,  split by '-' cause you got 'score - Initials'.
         * That way you have the score and the initials separeted.
         */
        for (int i = 0; i < scoreboardSplit.Length; i++)
        {
            try
            {
                if (!string.IsNullOrEmpty(scoreboardSplit[i]))
                    scoreboard.Add(scoreboardSplit[i].Split('-')[1], Convert.ToInt32(scoreboardSplit[i].Split('-')[0]));
            }

            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }

        #endregion

        #region Achievement

        if (!string.IsNullOrEmpty(achivementTxt))
        {
            achivementTxt = Cryptonic.DecryptString(Convert.FromBase64String(achivementTxt), Cryptonic.Key);

            string[] achiSplit = achivementTxt.Split(';');

            achievements.UnlockedCookie = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.COOKIE]);

            achievements.UnlockedFlame = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.FLAME]);

            achievements.UnlockedFlame1 = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.FLAME1]);

            achievements.UnlockedFlame2 = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.FLAME2]);

            achievements.UnlockedFlame3 = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.FLAME3]);

            achievements.UnlockedFlame4 = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.FLAME4]);

            achievements.UnlockedStreak = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.STREAK]);

            achievements.UnlockedStreak2 = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.STREAK2]);

            achievements.UnlockedStreak3 = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.STREAK3]);

            achievements.UnlockedStreak4 = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.STREAK4]);

            achievements.UnlockedWait = Convert.ToBoolean(achiSplit[(int)Achievement.SaveFileOrder.WAIT]);
        }

        #endregion

        #endregion

        #region Ball

        ball = new Ball()
        {
            Body = GetComponent<Rigidbody2D>(),

            ForceObj = new Force()
            {
                X = Force.LOWEST_FORCE,

                Y = Force.LOWEST_FORCE,

                MaxSpeed = scoreboard.Count > 0 ? scoreboard.ElementAt(0).Value : 5
            }
        }; 

        ball.Body.AddForce(new Vector2(ball.ForceObj.X, ball.ForceObj.Y));

        #endregion

        #region FXs

        audioSrcs = new List<AudioSource>(GetComponents<AudioSource>());

        musicSrc = GameObject.Find("MusicPlayer").GetComponent<AudioSource>();

        fxs = new FXs();

        fxs.FxDic.Add(audioSrcs.ElementAt((int)FXs.BallClips.FLAME1).clip, audioSrcs.ElementAt((int)FXs.BallClips.FLAME1));

        fxs.FxDic.Add(audioSrcs.ElementAt((int)FXs.BallClips.FLAME2).clip, audioSrcs.ElementAt((int)FXs.BallClips.FLAME2));

        fxs.FxDic.Add(audioSrcs.ElementAt((int)FXs.BallClips.FLAME3).clip, audioSrcs.ElementAt((int)FXs.BallClips.FLAME3));

        fxs.FxDic.Add(audioSrcs.ElementAt((int)FXs.BallClips.FLAME4).clip, audioSrcs.ElementAt((int)FXs.BallClips.FLAME4));

        fxs.FxDic.Add(audioSrcs.ElementAt((int)FXs.BallClips.FLAME5).clip, audioSrcs.ElementAt((int)FXs.BallClips.FLAME5));

        fxs.FxDic.Add(audioSrcs.ElementAt((int)FXs.BallClips.BALL).clip, audioSrcs.ElementAt((int)FXs.BallClips.BALL));

        //fxs.FxDic.Add((GameObject.Find("BtnSrc").GetComponent<AudioSource>()).clip,
        //    GameObject.Find("BtnSrc").GetComponent<AudioSource>());

        #endregion

        #region Animators

        burstAnim = GameObject.Find("BurstAnimObj").GetComponent<Animator>();

        #endregion

        //This is how you call other game objects 
        maxSpeedlbl = GameObject.Find("/Canvas/MaxSpeedLbl").GetComponent<Text>();

        streakLbl = GameObject.Find("/Canvas/StreakLbl").GetComponent<Text>();

        archiveLbl = GameObject.Find("/Canvas/AchievementLbl").GetComponent<Text>();

        #region Set last high score

        //Remove cause on the save file has filler string to avoid unique key issues
        string scoreboardKey = scoreboard.ElementAt(0).Key.Trim().Replace("*", string.Empty);

        maxSpeedlbl.text = string.Format("Max Speed \n {0} mph - {1}", ball.ForceObj.MaxSpeed,
            scoreboard.Count > 0
            ? scoreboardKey.Substring(0, scoreboardKey.Length < 3
            ? (scoreboardKey.Length)
            : 3)
            : "MAP"); 

        #endregion

        currentSpeedLbl = GameObject.Find("/Canvas/CurrentSpeedLbl").GetComponent<Text>();

        logLbl = GameObject.Find("/Canvas/LogLbl").GetComponent<Text>();

        #region Set references for menus

        //When they become invs they aren't accesable so made this to get a reference before they become inaccesable
        pausePan = GameObject.Find("/PauseCanvas/PausePan");

        #region Set build version txt on the lbl that contains build on the name

        foreach (var lbl in pausePan.GetComponentsInChildren<Text>())
        {
            if (lbl.name.Contains("Build"))
            {
                lbl.text = string.Format("Ver. {0}", GameWrapper.BUILD_NUMBER);

                break;
            }
        } 

        #endregion

        pausePan.SetActive(false);

        achiPan = GameObject.Find("/AchiCanvas/AchiPan");

        achiPan.SetActive(false);

        settingsPan = GameObject.Find("/SettingsCanvas/SettingsPan");

        settingsPan.SetActive(false);

        achiDetailPan = GameObject.Find("/AchiDetailCanvas/AchiDetailPan");

        achiDetailPan.SetActive(false);

        scorePan = GameObject.Find("/ScoreboardCanvas/ScorePan");

        scorePan.SetActive(false);

        assistPan = GameObject.Find("/PauseCanvas/AssistPan");

        assistPan.SetActive(false);

        #endregion

        #endregion
    }

    private void Start()
    {
        wrapper = MainMenuCanvas.GetWrapper();

        wrapper.Scoreboard = scoreboard;

        wrapper.Achievements = achievements;
    }

    // Update is called once per frame
    void Update ()
    {
        if(!string.IsNullOrEmpty(output))
            logLbl.text = string.Format("error: {0}; trace: {1}", output, stack);

        if (MainMenuCanvas.GetWrapper().HasStarted)
        {
            //Initialize wrapper obj on this class after the game starts
            if (wrapper == null)
            {
                wrapper = MainMenuCanvas.GetWrapper();

                wrapper.Scoreboard = scoreboard;

                wrapper.Achievements = achievements;
            }

            OnBackBtnPressed();

            #region Play flame fx according to what speed it's going and set it's reached flag

            #region Flame

            if (ball.ForceObj.MPH >= Force.SPEED && ball.ForceObj.MPH < Force.SPEED1)
            {
                /*
                         * mph might have increased before this so for it to only go in once, 
                         * I check the last recorded speed before the increase. 
                         * If it's true, means that it should play sound and do other things.
                         */
                if (ball.PreviousVelocityFrame < Force.SPEED)
                {
                    Animatron.SetAnimatorBoolParam(burstAnim, "PlayBurst", true);

                    if (wrapper.IsSoundOn)
                        fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME1).Value.PlayOneShot(
                            fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME1).Key);

                    if (!wrapper.Achievements.UnlockedFlame)
                    {
                        wrapper.Achievements.UnlockedFlame = !wrapper.Achievements.UnlockedFlame;

                        archiveUnlockCounter++;

                        wrapper.Achievements.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                        wrapper.Achievements.ActiveCurrentFrameCount = wrapper.Achievements.ActivePreviousFrameCount;
                    }
                }
            }

            #endregion

            #region Flame1

            else if (ball.ForceObj.MPH >= Force.SPEED1 && ball.ForceObj.MPH < Force.SPEED2)
            {
                if (ball.PreviousVelocityFrame < Force.SPEED1)
                {
                    Animatron.SetAnimatorBoolParam(burstAnim, "PlayBurst", true);

                    if (wrapper.IsSoundOn)
                        fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME2).Value.PlayOneShot(
                            fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME2).Key);

                    if (!wrapper.Achievements.UnlockedFlame1)
                    {
                        wrapper.Achievements.UnlockedFlame1 = !wrapper.Achievements.UnlockedFlame1;

                        archiveUnlockCounter++;

                        wrapper.Achievements.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                        wrapper.Achievements.ActiveCurrentFrameCount = wrapper.Achievements.ActivePreviousFrameCount;
                    }
                }
            }

            #endregion

            #region Flame2

            else if (ball.ForceObj.MPH >= Force.SPEED2 && ball.ForceObj.MPH < Force.SPEED3)
            {
                if (ball.PreviousVelocityFrame < Force.SPEED2)
                {
                    Animatron.SetAnimatorBoolParam(burstAnim, "PlayBurst", true);

                    if (wrapper.IsSoundOn)
                        fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME3).Value.PlayOneShot(
                            fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME4).Key);

                    if (!wrapper.Achievements.UnlockedFlame2)
                    {
                        wrapper.Achievements.UnlockedFlame2 = !wrapper.Achievements.UnlockedFlame2;

                        archiveUnlockCounter++;

                        wrapper.Achievements.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                        wrapper.Achievements.ActiveCurrentFrameCount = wrapper.Achievements.ActivePreviousFrameCount;
                    }
                }
            }

            #endregion

            #region Flame3

            else if (ball.ForceObj.MPH >= Force.SPEED3 && ball.ForceObj.MPH < Force.SPEED4)
            {
                if (ball.PreviousVelocityFrame < Force.SPEED3)
                {
                    Animatron.SetAnimatorBoolParam(burstAnim, "PlayBurst", true);

                    if (wrapper.IsSoundOn)
                        fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME4).Value.PlayOneShot(
                            fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME4).Key);

                    if (!wrapper.Achievements.UnlockedFlame3)
                    {
                        wrapper.Achievements.UnlockedFlame3 = !wrapper.Achievements.UnlockedFlame3;

                        archiveUnlockCounter++;

                        wrapper.Achievements.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                        wrapper.Achievements.ActiveCurrentFrameCount = wrapper.Achievements.ActivePreviousFrameCount;
                    }
                }
            }

            #endregion

            #region Flame4

            else if (ball.ForceObj.MPH >= Force.SPEED4)
            {
                if (ball.PreviousVelocityFrame < Force.SPEED4)
                {
                    Animatron.SetAnimatorBoolParam(burstAnim, "PlayBurst", true);

                    if (wrapper.IsSoundOn)
                        fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME5).Value.PlayOneShot(
                            fxs.FxDic.ElementAt((int)FXs.BallClips.FLAME5).Key);

                    if (!wrapper.Achievements.UnlockedFlame4)
                    {
                        wrapper.Achievements.UnlockedFlame4 = !wrapper.Achievements.UnlockedFlame4;

                        archiveUnlockCounter++;

                        wrapper.Achievements.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                        wrapper.Achievements.ActiveCurrentFrameCount = wrapper.Achievements.ActivePreviousFrameCount;
                    }
                }
            } 

            #endregion

            #endregion

            #region Update scoreboard

            //If the running score is higher than the high score, set the current score to high score
            if (ball.ForceObj.MPH > ball.ForceObj.MaxSpeed)
            {
                ball.ForceObj.MaxSpeed = ball.ForceObj.MPH;

                //Had to convert to list so it's easier to insert in the same spot as the high score
                List<KeyValuePair<string, int>> dicList = wrapper.Scoreboard.ToList();

                /*
                 * If the initials exist add one of these '*' to the initials so that there's no duplicate key error.
                 * So if you got 'tes' for inicials it'll be 'tes*'. Next time it updates it'll be 'tes**'. 
                 */
                if (wrapper.Scoreboard.ContainsKey(wrapper.UserInitials))
                {
                    dicList.Insert(0, new KeyValuePair<string, int>(wrapper.UserInitials + AddAditionalFillerCharsToString(),
                        ball.ForceObj.MaxSpeed));
                }

                //Mean there's no entry with those initials so add plain value
                else
                    dicList.Insert(0, new KeyValuePair<string, int>(wrapper.UserInitials, ball.ForceObj.MaxSpeed));

                wrapper.Scoreboard = dicList.ToDictionary((key) => key.Key, (value) => value.Value);
            }

            else if (ball.ForceObj.MPH < ball.ForceObj.MaxSpeed)
            {
                //Iterate through every score and check if it's not in the entries
                for (int i = 0; i < wrapper.Scoreboard.Count; i++)
                {
                    if (wrapper.Scoreboard.ElementAt(i).Value < ball.ForceObj.MPH)
                    {
                        //This is to avoid duplicates
                        if (!wrapper.Scoreboard.ContainsValue(ball.ForceObj.MPH))
                        {
                            //Same as when running score is higher then high score
                            List<KeyValuePair<string, int>> dicList = wrapper.Scoreboard.ToList();

                            if (wrapper.Scoreboard.ContainsKey(wrapper.UserInitials))
                                dicList.Insert(i, new KeyValuePair<string, int>(wrapper.UserInitials + AddAditionalFillerCharsToString()
                                , ball.ForceObj.MPH));

                            else
                                dicList.Insert(i, new KeyValuePair<string, int>(wrapper.UserInitials, ball.ForceObj.MPH));

                            wrapper.Scoreboard = dicList.ToDictionary((key) => key.Key, (value) => value.Value);

                            break;
                        }
                    }
                }
            }

            #endregion

            #region Lower speed based on frame count

            //Do this only when game not paused
            if (Time.timeScale > 0f)
            {
                //Increase counter so that velocity can stay constant for a longer period of time
                if (ball.FrameCount < Ball.FPS_COUNT_RESET)
                    ball.FrameCount++;

                else
                {
                    #region Reset counter, lower speed and change ball trail color

                    ball.FrameCount = 0;

                    if (ball.ForceObj.MPH > Force.LOWEST_MPH)
                    {
                        if (wrapper.Achievements.StreakCounter > 0)
                            ball.Body.velocity = Force.DecreaseVelocity(ball.Body, ball.ForceObj.AccumulatedSpeedFactor);

                        else
                            ball.Body.velocity = Force.DecreaseVelocity(Force.SPEED_FACTOR, ball.Body);

                        if (wrapper.Achievements.StreakCounter > 0)
                        {
                            ball.ForceObj.MPH -= wrapper.Achievements.AccumulatedStreak;

                            wrapper.Achievements.AccumulatedStreak = 0;

                            hitsCounter = wrapper.Achievements.AccumulatedStreak;

                            wrapper.Achievements.StreakCounter = hitsCounter;

                            ball.ForceObj.AccumulatedSpeedFactor = hitsCounter;

                            streakLbl.text = string.Empty;
                        }

                        ball.ForceObj.MPH--;

                        //bTrailColors.LerpCurrentTime = (Time.time - bTrailColors.LerpStartTime) * BallTrailColors.LERP_END_SPEED;

                        //ball.Force.MPH--;
                    } 

                    #endregion
                }
            }

            #endregion

            var po = (GameObject.Find("BurstAnimObj"));

            po.transform.position = new Vector3(ball.Body.position.x, ball.Body.position.y, po.transform.position.z);

            ball.PreviousVelocityFrame = ball.ForceObj.MPH;

            //* is used to identify a new key on the dic so there's no not unique key error
            string scoreboardKey = wrapper.Scoreboard.ElementAt(0).Key.Trim().Replace("*", string.Empty);

            maxSpeedlbl.text = string.Format("Max Speed \n {0} mph - {1}", wrapper.Scoreboard.ElementAt(0).Value,
                scoreboardKey.Substring(0, scoreboardKey.Length < 3 ? (scoreboardKey.Length) : 3));

            currentSpeedLbl.text = string.Format("{0} mph", ball.ForceObj.MPH);

            #region Display archive lbl and flicker it

            if (wrapper.Achievements.ActivePreviousFrameCount > 0)
            {
                int ActivePreviousFrameCount = wrapper.Achievements.ActivePreviousFrameCount,
                    ActiveCurrentFrameCount = wrapper.Achievements.ActiveCurrentFrameCount,
                    fpsDiff; // Has the substraction of current and previous frame counts

                ActiveCurrentFrameCount--;

                fpsDiff = ActivePreviousFrameCount - ActiveCurrentFrameCount;

                //If the difference == the indicator measn it should change flicker state
                if (fpsDiff == Achievement.FLICKER_INDICATOR && ActiveCurrentFrameCount > 0)
                {
                    ActivePreviousFrameCount = ActiveCurrentFrameCount;

                    //This makes the flicker effect. 
                    archiveLbl.enabled = !archiveLbl.enabled;
                }

                else if (ActiveCurrentFrameCount == 0)
                {
                    archiveLbl.enabled = true;

                    archiveUnlockCounter = 0;
                }

                // This is set to global variable cause this all runs on a loop and comes back in  if active
                wrapper.Achievements.ActivePreviousFrameCount = ActivePreviousFrameCount;

                wrapper.Achievements.ActiveCurrentFrameCount = ActiveCurrentFrameCount;
            }

            if (archiveUnlockCounter > 0)
                archiveLbl.text = string.Format("[+{0}] Archive", archiveUnlockCounter);

            else
                archiveLbl.text = string.Empty; 

            #endregion
        }
    }

    //Play ball bounce fx
    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch(collision.gameObject.tag)
        {
            case "LBound":

                if (wrapper == null || wrapper.IsSoundOn)
                    fxs.FxDic.ElementAt((int)FXs.BallClips.BALL).Value.PlayOneShot(fxs.FxDic.ElementAt((int)FXs.BallClips.BALL).Key,
                        FXs.GetRandomVol());

                break;

            case "RBound":

                if (wrapper == null || wrapper.IsSoundOn)
                    fxs.FxDic.ElementAt((int)FXs.BallClips.BALL).Value.PlayOneShot(fxs.FxDic.ElementAt((int)FXs.BallClips.BALL).Key,
                        FXs.GetRandomVol());

                break;

            case "UBound":

                if (wrapper == null || wrapper.IsSoundOn)
                    fxs.FxDic.ElementAt((int)FXs.BallClips.BALL).Value.PlayOneShot(fxs.FxDic.ElementAt((int)FXs.BallClips.BALL).Key,
                        FXs.GetRandomVol());

                break;

            case "BBound":

                if (wrapper == null || wrapper.IsSoundOn)
                    fxs.FxDic.ElementAt((int)FXs.BallClips.BALL).Value.PlayOneShot(fxs.FxDic.ElementAt((int)FXs.BallClips.BALL).Key,
                        FXs.GetRandomVol());

                break;
        }
    }

    private void OnMouseDown()
    {
        //If you click the ball while on main menu or any other menu you could get points
        if (MainMenuCanvas.GetWrapper().HasStarted && Time.timeScale > 0f)
        {
            //Store current datetime
            currentDate = DateTime.Now;

            Achievement ach = wrapper.Achievements;

            #region Check waiting achievement

            if (!ach.UnlockedWait)
            {
                //On the first click it's not true cause lastclick is set to max value and this returns a negative value
                if (TimeSpan.Compare(currentDate.TimeOfDay, currentDateAfter30secs.TimeOfDay) >= 0)
                {
                    ach.UnlockedWait = !ach.UnlockedWait;

                    archiveUnlockCounter++;

                    ach.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                    ach.ActiveCurrentFrameCount = ach.ActivePreviousFrameCount;
                }
            }

            #endregion

            //Store the previous click datetime
            currentDateAfter30secs = currentDate.AddSeconds(Achievement.WAIT_DURATION);

            //This is the cookie achievement
            if (!ach.UnlockedCookie)
            {
                ach.UnlockedCookie = !ach.UnlockedCookie;

                archiveUnlockCounter++;

                ach.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                ach.ActiveCurrentFrameCount = ach.ActivePreviousFrameCount;
            }

            #region Streak achievements, counters and accumulators

            hitsCounter++;

            if (hitsCounter >= Achievement.HITS_TO_STREAK)
            {
                ach.StreakCounter++;

                streakLbl.text = string.Format("(x{0})", ach.StreakCounter);

                if (ach.StreakCounter == Achievement.TOTAL_FOR_STREAK && !ach.UnlockedStreak)
                {
                    ach.UnlockedStreak = !ach.UnlockedStreak;

                    archiveUnlockCounter++;

                    ach.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                    ach.ActiveCurrentFrameCount = ach.ActivePreviousFrameCount;
                }

                if (ach.StreakCounter == Achievement.TOTAL_FOR_STREAK2 && !ach.UnlockedStreak2)
                {
                    ach.UnlockedStreak2 = !ach.UnlockedStreak2;

                    archiveUnlockCounter++;

                    ach.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                    ach.ActiveCurrentFrameCount = ach.ActivePreviousFrameCount;
                }

                if (ach.StreakCounter == Achievement.TOTAL_FOR_STREAK3 && !ach.UnlockedStreak3)
                {
                    ach.UnlockedStreak3 = !ach.UnlockedStreak3;

                    archiveUnlockCounter++;

                    ach.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                    ach.ActiveCurrentFrameCount = ach.ActivePreviousFrameCount;
                }

                if (ach.StreakCounter == Achievement.TOTAL_FOR_STREAK4 && !ach.UnlockedStreak4)
                {
                    ach.UnlockedStreak4 = !ach.UnlockedStreak4;

                    archiveUnlockCounter++;

                    ach.ActivePreviousFrameCount = Achievement.DURATION_FLICKER;

                    ach.ActiveCurrentFrameCount = ach.ActivePreviousFrameCount;
                }

                ball.ForceObj.MPH += ach.StreakCounter;

                ach.AccumulatedStreak += ach.StreakCounter;

                ball.ForceObj.AccumulatedSpeedFactor += GameWrapper.IntToFloat(ach.StreakCounter);
            } 

            #endregion

            //If there's a streak it'll always add one to speed
            ball.Body.velocity = Force.VelocityInverter(ball.Body,
                (GameWrapper.IntToFloat(ach.StreakCounter) + Force.SPEED_FACTOR));

            //Update main achievements obj
            wrapper.Achievements = ach;

            ball.ForceObj.MPH++;

            //Reset frame count
            ball.FrameCount = 0;
        }        
    }

    private void OnApplicationPause(bool pause)
    {
        if(pause)
        {
            string scoreboardFileTxt = string.Empty,
                achievementsFileTxt = string.Empty;

            //If null cause sometimes this is called before the game starts
            if (wrapper != null)
            {
                #region Scoreboard

                //Get all dictionary entris and store on string
                for (int i = 0; i < wrapper.Scoreboard.Count; i++)
                {
                    scoreboardFileTxt += string.Format("{0} - {1};", wrapper.Scoreboard.ElementAt(i).Value,
                        wrapper.Scoreboard.ElementAt(i).Key.Trim());
                } 

                #endregion

                achievementsFileTxt = ConvertAchievementsToString(wrapper.Achievements);
            }

            string encryptedScore = Convert.ToBase64String(Cryptonic.EncryptString(scoreboardFileTxt, Cryptonic.Key)),
                encryptedAchiv = Convert.ToBase64String(Cryptonic.EncryptString(achievementsFileTxt, Cryptonic.Key));

            //encrypted = Cryptonic.DecryptString(Convert.FromBase64String(encrypted), Cryptonic.Key);

            FileManager.Write(encryptedScore, Force.SAVE_FILE_NAME);

            FileManager.Write(encryptedAchiv, Achievement.SAVE_FILE_NAME);
        }
            //FileManager.Write(ball.Force.MaxSpeed.ToString(), "MaxSpeed");
    }

    /// <summary>
    /// Shortcut to concat all achievement bool values to a string
    /// </summary>
    /// <param name="ach"></param>
    /// <returns></returns>
    public static string ConvertAchievementsToString(Achievement ach)
    {
        string conversion = string.Empty;

        conversion = ach.UnlockedCookie.ToString();

        conversion += string.Format(";{0}", ach.UnlockedFlame.ToString());

        conversion += string.Format(";{0}", ach.UnlockedFlame1.ToString());

        conversion += string.Format(";{0}", ach.UnlockedFlame2.ToString());

        conversion += string.Format(";{0}", ach.UnlockedFlame3.ToString());

        conversion += string.Format(";{0}", ach.UnlockedFlame4.ToString());

        conversion += string.Format(";{0}", ach.UnlockedStreak.ToString());

        conversion += string.Format(";{0}", ach.UnlockedStreak2.ToString());

        conversion += string.Format(";{0}", ach.UnlockedStreak3.ToString());

        conversion += string.Format(";{0}", ach.UnlockedStreak4.ToString());

        conversion += string.Format(";{0}", ach.UnlockedWait.ToString());

        return conversion;
    }

    public static List<AudioSource> GetFXAudioSrcs()
    {
        return audioSrcs;
    }

    public static void SetFXAudioSrc(List<AudioSource> srcs)
    {
        audioSrcs = srcs;
    }

    public static AudioSource GetMusicAudioSrc()
    {
        return musicSrc;
    }

    public static void SetMusicAudioSrc(AudioSource src)
    {
        musicSrc = src;
    }

    public static GameWrapper GetGameWrapper()
    {
        return wrapper;
    }

    public static void SetGameWrapper(GameWrapper w)
    {
        wrapper = w;
    }

    /// <summary>
    /// Kill the app on back pressed
    /// </summary>
    private void OnBackBtnPressed()
    {
        if (Time.timeScale > 0 && Input.GetKeyDown(KeyCode.Escape))
        {
            //Open the pause menu
            PauseMenu.PauseClick(pausePan, achiPan, settingsPan, achiDetailPan, scorePan, assistPan);
            //PauseMenu.PauseClick(ball, pausePan, achiPan, scrollPan);
        }
    }

    private string AddAditionalFillerCharsToString()
    {
        int longestCharFillerCount = 0; //Hold the longest amount of filler chars in a string

        //Iterate through all the dictionary and find the highest amount of filler chars
        for (int i = 0; i < wrapper.Scoreboard.Count; i++)
        {
            //if (wrapper.Scoreboard.ElementAt(i).Key.Contains(wrapper.UserInitials)
            //    && wrapper.Scoreboard.ElementAt(i).Key.Length > longestDuplicatedNameLength)
            //{
            //    longestDuplicatedNameLength = wrapper.Scoreboard.ElementAt(i).Key.Length;
            //}

            if (wrapper.Scoreboard.ElementAt(i).Key.Contains(wrapper.UserInitials))
            {
                //Holds the amount of filler chars in the current index
                int fillerCharsCount = wrapper.Scoreboard.ElementAt(i).Key.Length -
                    wrapper.Scoreboard.ElementAt(i).Key.Replace("*", string.Empty).Length;

                if(fillerCharsCount > longestCharFillerCount)
                    longestCharFillerCount = fillerCharsCount;
            }

        }

        string userInitialFiller = "*";

        //for (int i = 0; i <= (longestDuplicatedNameLength - 4); i++)

        //Build filler string based on the highest count. For example, if the highest is 3 this new filler will have highest +1
        for (int i = 0; i < (longestCharFillerCount); i++)
        {
            userInitialFiller += "*";
        }

        return userInitialFiller;
    }

    private void OnDisable()
    {
        Application.logMessageReceived += HandleLog;
    }

    private void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

     void HandleLog(string logString, string stackTrace, LogType type)
    {
        switch(type)
        {
            case LogType.Error:

                output = logString;

                stack = stackTrace;

                break;

            case LogType.Exception:

                output = logString;

                stack = stackTrace;

                break;
        }
    }

}
