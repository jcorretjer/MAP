﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.C_.Ball.Models
{
    /// <summary>
    /// Holds all the physics
    /// </summary>
    public class Force
    {
        public const float MPH_MULTIPLIER = 2.2369f,
            SPEED_FACTOR = 0.3f,
            LOWEST_FORCE = 151f;

        //public const int SPEED = 20,
        //    SPEED1 = 30,
        //    SPEED2 = 40,
        //    SPEED3 = 50,
        //    SPEED4 = 60,
        public const int SPEED = 10,
            SPEED1 = 30,
            SPEED2 = 60,
            SPEED3 = 80,
            SPEED4 = 100,
            LOWEST_MPH = 5;

        public const string SAVE_FILE_NAME = "MaxSpeed";

        public Force()
        {
            X = 0f;

            Y = 0f;

            MaxSpeed = LOWEST_MPH;

            ReachedSpeed = false;

            ReachedSpeed1 = false;

            ReachedSpeed2 = false;

            ReachedSpeed3 = false;

            ReachedSpeed4 = false;

            MPH = LOWEST_MPH;

            AccumulatedSpeedFactor = 0;
        }

        public float AccumulatedSpeedFactor
        {
            get;

            set;
        }

        public float X
        {
            get;

            set;
        }

        public float Y
        {
            get;

            set;
        }

        public bool ReachedSpeed
        {
            get;

            set;
        }

        public bool ReachedSpeed1
        {
            get;

            set;
        }

        public bool ReachedSpeed2
        {
            get;

            set;
        }

        public bool ReachedSpeed3
        {
            get;

            set;
        }

        public bool ReachedSpeed4
        {
            get;

            set;
        }

        public int MPH
        {
            get;

            set;
        }

        public int MaxSpeed
        {
            get;

            set;
        }

        /// <summary>
        /// Lowers the velocities x and y axis of a rigid body based on the factor
        /// </summary>
        /// <param name="factor">Value used for decreasing</param>
        /// <param name="body">Body wished to be slowed down</param>
        /// <returns>The new velocity vector2</returns>
        public static Vector2 DecreaseVelocity(float factor, Rigidbody2D body)
        {
            Vector2 velocity = body.velocity;

            //Slowdown speed if time isn't stopped
            if (Time.timeScale > 0)
            {
                if (velocity.x > 0)
                {
                    velocity.x -= factor;
                }

                else
                {
                    velocity.x += factor;
                }

                if (velocity.y > 0)
                {
                    velocity.y -= factor;
                }

                else
                {
                    velocity.y += factor;
                }
            }

            return velocity;
        }

        /// <summary>
        /// Substracts the added speed from streak.
        /// </summary>
        /// <param name="body"></param>
        /// <param name="accomolatedStreaks">Total values added to speed cause of streak</param>
        /// <returns></returns>
        public static Vector2 DecreaseVelocity(Rigidbody2D body, float accomolatedStreaks)
        {
            Vector2 velocity = body.velocity;

            //float factor = velocity.x - accomolatedStreaks;

            //Slowdown speed if time isn't stopped
            if (Time.timeScale > 0)
            {
                if (velocity.x > 0)
                {
                    velocity.x -= accomolatedStreaks; ;
                }

                else
                {
                    velocity.x += accomolatedStreaks;
                }

                if (velocity.y > 0)
                {
                    velocity.y -= accomolatedStreaks;
                }

                else
                {
                    velocity.y += accomolatedStreaks;
                }
            }

            return velocity;
        }

        /// <summary>
        /// Flips a rigidbodys x and y velocity polarity. Also, increases its velocity based of a factor. 
        /// </summary>
        /// <param name="body">Body used for inversion</param>
        /// <param name="factor">Factor that increases velocity</param>
        /// <returns>New inverted velocity</returns>
        public static Vector2 VelocityInverter(Rigidbody2D body, float factor)
        {
            Vector2 velocity = body.velocity;

            if (velocity.x > 0)
            {
                velocity.x *= -1;

                velocity.x -= factor;
            }

            else
            {
                velocity.x = Mathf.Abs(velocity.x);

                velocity.x += factor;
            }

            if (velocity.y > 0)
            {
                velocity.y *= -1;

                velocity.y -= factor;
            }

            else
            {
                velocity.y = Mathf.Abs(velocity.y);

                velocity.y += factor;
            }

            return velocity;
        }
    }
}
